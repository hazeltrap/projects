﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace KatarzynaKrawczykLab3
{
    public partial class Main : Form
    {
        SqlConnection connection;
        public Main()
        {
            InitializeComponent();
            connection = new SqlConnection(@"Data Source =LAPTOP-0QBN1NML\FRYGTT; database=Pizzeria; Trusted_Connection=yes");
            
        }
        /// <summary>
        /// wyświetla zawartość tabeli Pizzas 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonShow_Click(object sender, EventArgs e)
        {
            SqlDataAdapter adapter = new SqlDataAdapter($"SELECT * FROM Pizzas", connection);
            DataTable table = new DataTable();

            adapter.Fill(table);
            dataGridViewPizzas.DataSource = table;
        }
        /// <summary>
        /// wyświetla te pizze które mają cenę niższą niż cena wpisana w textBoxPrice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPrice_Click(object sender, EventArgs e) //
        {
            SqlDataAdapter adapter = new SqlDataAdapter($"SELECT * FROM Pizzas WHERE Price < 0{textBoxPrice.Text}", connection);
            DataTable table = new DataTable();

            adapter.Fill(table);
            dataGridViewPizzas.DataSource = table;
        }
    }
}
