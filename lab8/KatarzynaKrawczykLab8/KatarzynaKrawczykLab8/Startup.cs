﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KatarzynaKrawczykLab8.Startup))]
namespace KatarzynaKrawczykLab8
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
