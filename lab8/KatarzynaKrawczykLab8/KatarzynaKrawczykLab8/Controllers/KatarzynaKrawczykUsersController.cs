﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KatarzynaKrawczykLab8.Models;

namespace KatarzynaKrawczykLab8.Controllers
{
    public class KatarzynaKrawczykUsersController : Controller
    {
        private DB_9B1FC5_cpc20181Entities1 db = new DB_9B1FC5_cpc20181Entities1();

        // GET: KatarzynaKrawczykUsers
        public ActionResult Index()
        {
            return View(db.KatarzynaKrawczykUsers.ToList());
        }

        // GET: KatarzynaKrawczykUsers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KatarzynaKrawczykUsers katarzynaKrawczykUsers = db.KatarzynaKrawczykUsers.Find(id);
            if (katarzynaKrawczykUsers == null)
            {
                return HttpNotFound();
            }
            return View(katarzynaKrawczykUsers);
        }

        // GET: KatarzynaKrawczykUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KatarzynaKrawczykUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Login,Password,Name,Surname")] KatarzynaKrawczykUsers katarzynaKrawczykUsers)
        {
            if (ModelState.IsValid)
            {
                db.KatarzynaKrawczykUsers.Add(katarzynaKrawczykUsers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(katarzynaKrawczykUsers);
        }

        // GET: KatarzynaKrawczykUsers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KatarzynaKrawczykUsers katarzynaKrawczykUsers = db.KatarzynaKrawczykUsers.Find(id);
            if (katarzynaKrawczykUsers == null)
            {
                return HttpNotFound();
            }
            return View(katarzynaKrawczykUsers);
        }

        // POST: KatarzynaKrawczykUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Login,Password,Name,Surname")] KatarzynaKrawczykUsers katarzynaKrawczykUsers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(katarzynaKrawczykUsers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(katarzynaKrawczykUsers);
        }

        // GET: KatarzynaKrawczykUsers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KatarzynaKrawczykUsers katarzynaKrawczykUsers = db.KatarzynaKrawczykUsers.Find(id);
            if (katarzynaKrawczykUsers == null)
            {
                return HttpNotFound();
            }
            return View(katarzynaKrawczykUsers);
        }

        // POST: KatarzynaKrawczykUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KatarzynaKrawczykUsers katarzynaKrawczykUsers = db.KatarzynaKrawczykUsers.Find(id);
            db.KatarzynaKrawczykUsers.Remove(katarzynaKrawczykUsers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
