﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaKrawczykLab1
{

    public partial class Form1 : Form
    {
        int counter=0;
       
        public Form1()
        {
            InitializeComponent();
            timerCount.Start();
        }


        private void buttonChangeColor_Click(object sender, EventArgs e) /// funkcja zmieniajaca kolor po kliknieciu przycisku
        {
            this.BackColor = Color.LightSkyBlue;
            textBoxChange.BackColor = Color.LightPink;
            textBoxChange.Text = "xyz";
        }

        private void buttonReturn_Click(object sender, EventArgs e)
        {
            this.BackColor = SystemColors.Control;
            textBoxChange.BackColor = SystemColors.Control;
            MessageBox.Show(textBoxChange.Text);
            textBoxChange.Clear();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonShow_Click(object sender, EventArgs e)
        {
            int number1, number2, result;
            number1 = Int32.Parse(textBoxNumber1.Text);
            number2 = Int32.Parse(textBoxNumber2.Text);
            result = number1 + number2;
            MessageBox.Show(result.ToString()+"!");

        }

        private void buttonLoop_Click(object sender, EventArgs e)
        {
            int wynik=0;
            int number = Int32.Parse(textBoxNumber3.Text);
            wynik = number;
            for (int i=0; i<3; i++)
            {
                wynik = wynik * number;
            }
            MessageBox.Show(wynik.ToString() + "!");
        }

        private void timerCount_Tick(object sender, EventArgs e)
        {
            counter++;
            if (counter == 5)
               MessageBox.Show("!!!");
        }
    }
}
//W DOMU
//program symuljący działanie fabryki/sklepu magazynu. W programie co określony czas będą zmieniać się wartości. (clicker) 
//nie zrobic błędu (nazwy, komentarze przy funkcjach i zmiennych) 
//Termin - Niedziela do 1:00. (Programistyczny wiczór) (Na repozytorium).  