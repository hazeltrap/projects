﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaKrawczykLab4.Models
{
    public class Review: Entity //recenzja
    {
        [Required]//pole poniżej jest wymagane
        public string Text { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public int Score { get; set; }

        [ForeignKey("GameId")] //które pole jest kluczem obcym
        public virtual Game Game { get; set; }
        public int GameId { get; set; } //pole klucza obcego

    }
}
