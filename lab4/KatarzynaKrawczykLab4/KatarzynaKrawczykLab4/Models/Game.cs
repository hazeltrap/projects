﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KatarzynaKrawczykLab4.Models
{
    public class Game: Entity //gra
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Producer { get; set; }

        public virtual List<Review> Reviews { get; set; }
    }
}
