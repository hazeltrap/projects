﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaKrawczykLab4.Models
{
    public abstract class Entity //dziedziczenie daje każdej klasie dziedziczącej id i klucz główny 
    {
        [Key]
        //autoinkremenacja (powieksza klejne id o jeden)
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}
