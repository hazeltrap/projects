﻿using KatarzynaKrawczykLab4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaKrawczykLab4.Repositories
{
    public class RWRepository<T> : IRWRepository<T> where T : Entity
    {
        private readonly Models.AppContext context;
        public RWRepository(Models.AppContext context)
        {
            this.context= context;
        }

        public void Create(T entity)
        {
            context.Set<T>().Add(entity);
            context.SaveChanges();
        }

        public void Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public List<T> GetAll()
        {
            return context.Set<T>().ToList();
        }

        public T GetById()
        {
            throw new NotImplementedException();
        }

        public T GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
