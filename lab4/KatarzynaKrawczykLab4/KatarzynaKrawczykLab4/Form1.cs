﻿using KatarzynaKrawczykLab4.Models;
using KatarzynaKrawczykLab4.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaKrawczykLab4
{
    public partial class Form1 : Form
    {
        private readonly Models.AppContext context;
        public RWRepository<Game> Games;

        public Form1()
        {
            InitializeComponent();
            context = new Models.AppContext();
            Games = new RWRepository<Game>(context);
        }
        /// <summary>
        /// pobieranie danych z bazy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            GeatAllDataAboutGamesToDataGridView();
        }
        private void GeatAllDataAboutGamesToDataGridView()
        {
            dataGridViewGames.DataSource = Games.GetAll().Select(
                x => new
                {
                    Id=x.Id, 
                    Name = x.Name,
                    Producer = x.Producer
                }
                ).ToList();
        }

        private void buttonAddGame_Click(object sender, EventArgs e)
        {
            string name = textBoxName.Text;
            string producer = textBoxProducer.Text;
            if(name != String.Empty && producer != String.Empty)
            {
                Game newGame = new Game()
                {
                    Name = name,
                    Producer = producer
                };

                Games.Create(newGame);
                MessageBox.Show("Added");
            }
            else
            {
                MessageBox.Show("Wrong data!");
            }
        }
    }
}
