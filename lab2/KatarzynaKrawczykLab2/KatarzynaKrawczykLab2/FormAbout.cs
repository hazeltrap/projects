﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaKrawczykLab2
{
    public partial class FormAbout : Form
    {
        /// <summary>
        /// nazwa do przekazania
        /// </summary>
        public string frogName; 

        public FormAbout()
        {
            InitializeComponent();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Increment(1);
            labelTitle.Text = "Pociesz żabke " + frogName;
        }
    }
}
