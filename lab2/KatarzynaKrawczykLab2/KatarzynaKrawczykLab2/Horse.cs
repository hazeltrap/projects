﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaKrawczykLab2
{
    class Horse: Creature, IMovable
    {
        public string Name { get; set; } //pole
        public string FavouriteColor { get; set; }  //properties

        public Horse()
        { }
        /// <summary>
        /// Konstruktor PARAMETROWY
        /// </summary>
        /// <param name="newName">nowe imie dla konia</param>
        /// <param name="favouriteColor">nowy ulubiony kolor</param>
        
        public Horse(string newName, string favouriteColor)
        {
            Name = newName;
            FavouriteColor = favouriteColor;
        }

        public string DoSomething()
        {
            return "I did something";
        }
        /// <summary>
        /// Zwracanie informcji o obiekcie
        /// </summary>
        /// <returns>String - informacje o obiekcie</returns>
        override
            public string ToString()
        {
            return $"My name is {Name} and my favourite ccolor is {FavouriteColor}.";
        }

        public void Go()
        {

        }
    }
  
}
/*
 * Abstract Class a Interface
 * class Horse: ParentCreature, Interface1, Interface2
 * 
 * Interface - zbiór metod które muszą być w klasie
 * public Interface Interfacce1
 * {
 *      Move(); 
 * }
 */