﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaKrawczykLab2
{
    public partial class Form1 : Form
    {
        FormAbout formAbout;

        public Form1()
        {
            InitializeComponent();
            textBoxConsole.ScrollBars = ScrollBars.Horizontal;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            formAbout = new FormAbout();
            formAbout.frogName = "Malgosie";
            formAbout.Show();
           
        }

        /// <summary>
        /// Dodawanie tekstu do textBoxConsole
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonShow_Click(object sender, EventArgs e)
        {
            Horse horse = new Horse("Antont", "Black");
            textBoxConsole.Text += (horse.GetCurrentSpeed().ToString()+"\n");
        }
    }
}
