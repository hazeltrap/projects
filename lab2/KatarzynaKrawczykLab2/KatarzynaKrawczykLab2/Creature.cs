﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaKrawczykLab2
{
    abstract class Creature
    {
        int MaxSpeed { get; set; }
        Random random = new Random();

        public virtual  int GetCurrentSpeed()
        {
            int currentSpeed = random.Next(MaxSpeed);
            return currentSpeed; 
        }
    }
}
