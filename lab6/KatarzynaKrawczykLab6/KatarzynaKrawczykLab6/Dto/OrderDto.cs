﻿namespace KatarzynaKrawczykLab6.Dto
{
    //model zamówienia
    public class OrderDto
    {
        public int OrderId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}